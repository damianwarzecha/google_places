let places = [
    {id: 1, name: "Bazylika Mariacka", localization: "plac Mariacki 5, 31-042 Kraków"},
    {id: 2, name: "Sukiennice", localization: "Rynek Główny 1-3, 30-001 Kraków"},
    ];


let id = 2;

function getPlace(id) {

    id = parseInt(id);

    findPlaceById(id);

}

function findPlaceById(id) {

    let place = null;
    id = parseInt(id);

    places.every(function(p) {

        if(p.id === id) {
            place = p;
            return false;
        }
        return true;
    });

    return place;
}

function addPlace(placeData) {

    placeData.id = ++id;

    places.push(placeData);

}



function deletePlace(id) {

    let place = findPlaceById(id),
        index = places.indexOf(place);
    places.splice(index, 1);
}

function listPlaces() {

    return places;
}

function updatePlace(placeData) {

    let place = findPlaceById(placeData.id);

    delete placeData.id;

    Object.assign(place, placeData);

    console.log(places);

    // return getUser(user.id);

}


module.exports = {
    add: addPlace,
    get: getPlace,
    update: updatePlace,
    delete: deletePlace,
    list: listPlaces
};