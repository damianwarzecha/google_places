/**
 * Funkcja wysyłająca żądanie DELETE do api
 * @param id - id miejsca
 */
function deletePlace(id) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log("OK");
        }
    };

    xhttp.open("DELETE", "/api/"+id, true);
    xhttp.send();

    window.location.replace(`/api`);
}

/**
 * Funkcja aktualizująca dane metodą PUT
 * @param data - dane miejsca
 */
function updatePlace(data) {

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            console.log("OK");
        }
    };

    xhttp.open("PUT", "/api/"+data.id, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify({name: data.name, localization: data.localization}));

    window.location.replace(`/api`);
}

/**
 * Funkcja dodająca metodą POST
 * @param data - dane miejsca
 */
function addPlace(data) {

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            console.log("OK");
        }
    };
    xhttp.open("POST", "/api", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify({name: data.name, localization: data.localization}));

    window.location.replace(`/api`);
}

/**
 * Funkcja wyświetlająca formularz edycji
 * @param id - id tagu html
 */
function editPlace(id) {

    let el = document.getElementById(id);
    let input = document.getElementById("edit"+id);

    el.style.display = 'none';
    input.style.display = '';

}

/**
 * Funkcja ukrywająca możliwość edycji
 * @param id - id tagu html
 */
function cencelEditPlace(id) {

    let el = document.getElementById(id);
    let input = document.getElementById("edit"+id);

    el.style.display = '';
    input.style.display = 'none';
}

/**
 * Funkcja pobierająca dane z formularza edycji
 * @param id - id tagu html
 */
function getDataToEditPlace(id) {
    let input = document.querySelectorAll("#edit"+id+" > input");
    let data = [];

    data.name = input[0].value;
    data.localization = input[1].value;
    data.id = id;

    updatePlace(data);
}

/**
 * Funkcja pobierająca dane z formularza dodawania nowej pozycji
 */
function getDataToAddPlace() {

    let input = document.querySelectorAll("#CRUD > div > input");
    let data = [];

    data.name = input[0].value;
    data.localization = input[1].value;

    addPlace(data);
}