(function () {
    /**
     * załadowanie strony
     */
    window.onload = function () {

        /**
         * dodanie pozycji w historii widoku listy miejsc
         */
        window.history.replaceState({url : "tiles"}, "", "");

        /**
         * @type {boolean} - flaga blokująca wysyłanie żądań do serwera po ponownym scrollowaniu
         */
        let flag = true;
        /**
         * @type {null} - pozycja scrolla
         */
        let scroll = null;
        /**
         * @type {Array} - pobranie url na rzecz wyłuskania danych wprowadzonych przez klienta
         */
        let url = window.location.toString().replace("https://damianwarzecha.pl/send/", "").split("/");
        /**
         * @type {string} - pobranie danych do zmiennych z tablicy url
         */
        let loc = decodeURI(url[0]),
            type = url[1],
            radius = url[2].match("\\d+");
        /**
         * Wyświetlenie animacji ładowania danych
         */
        removeHidden(".loading");


        /**
         * Doładowywanie kolejnych danych po przewinięciu do konca strony
         */
        window.addEventListener("scroll", function () {
            console.log(flag);
            if (((document.body.scrollHeight - window.innerHeight) - 20 <= window.pageYOffset) && flag) {
                scroll = window.pageYOffset;
                flag = false;
                removeHidden(".loading");
                removeHidden(".outer");
                socket.emit("result");
            }
        });


        /**
         * Wysłanie żadania do serwera z danymi do przetwarzania
         */
        socket.emit("result", {
            loc : loc,
            type : type,
            radius : radius
        });

        /**
         * Odebranie danych z serwera i przetwarzanie w celu wyświetlenia
         */
        socket.on("checkRes", function (data) {
            /**
             * Operacje wykonywane jeśli data to obiekt oraz zawiera dane szczegółowe
             */
            if (!data.isString && data.det!==null){
                /**
                 * @type {Element} - kontener na dane w widoku listy
                 */
                let tilesContainer = document.querySelector(".tilesContainer");
                /**
                 * Przetwarzanie w petli danych
                 */
                data.det.forEach(function (item) {
                    /**
                     * Dodanie kolejnego elementu listy
                     */
                    tilesContainer.insertAdjacentHTML("beforeend", tilesHtml(item));
                    /**
                     * Dodanie danych do widoku szczegółowego
                     */
                    tilesDetails(item.result, ".detailsPlaces");

                });

                /**
                 * Dodanie obsługi kliknięcia
                 */
                clickTiles();
                /**
                 * ukrycie okna ładowania
                 */
                addHidden(".loading");
                addHidden(".outer");

                /**
                 * @type {boolean} - zmiana flagi blokującej ładowanie po scrollowaniu
                 */
                flag = true;
                /**
                 * @type {number} - ilość kroków przewijania
                 */
                let scrollStep = 0;
                /**
                 * @type {Object} - pętla uruchamiająca przewijanie
                 */
                let interval = setInterval(function () {

                    if (scroll !== null){

                        scroll = scroll+(++scrollStep);
                        window.scrollTo(0, scroll);
                        if (scrollStep===20){
                            clearInterval(interval);
                        }

                    }

                },20);

            }

            /**
             * Po przetworzeniu danych zmienia flage blokującą ładowanie po scrollowaniu.
             * Jeśli wystąpi błąd lub API Google zwróci zero wyników zostanie wyświetlony
             * alert oraz przekierowanie do strony głównej
             */
            if (data.loadNext === "END"){
                flag = false;
            } else if (data.loadNext === "ZERO_RESULTS"){
                alert("Zero wyników");
                window.location.replace(`/`);
            } else if (data.loadNext === "ERROR"){
                alert("ERROR");
                window.location.replace(`/`);
            }
        });

    };
    /**
     * Uruchomienie przetwarzania historii
     */
    myhistory();

})();

/**
 * Funkcja pokazująca dany element
 * @param element - selector elementu
 */
function removeHidden(element) {
    document.querySelector(`${element}`).classList.remove("hidden");
}
/**
 * Funkcja ukrywająca dany element
 * @param element - selector elementu
 */
function addHidden(element) {
    document.querySelector(`${element}`).classList.add("hidden");
}