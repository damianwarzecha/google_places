/**
 * @param data - dane szczegołowe pobrane z google API
 * @returns {string} - zwraca obiekt DOM kafelka do widoku listy
 */
function tilesHtml(data) {

    /**
     * Sprawdzenie dostępności fotografi z google API
     */
    if (data.photoUrl !== undefined){
        var photo = data.photoUrl;
    } else {
        var photo = "/img/foto.gif";
    }

    let tilesWithPhoto= `<div class="col-md-4, col-sm-4" id="${data.result.place_id}">
                                <div class="tiles">
                                    <img class="img-thumbnail center-block" src=${photo}>
                                    <div class="describe">
                                        <p>${data.result.name}</p>
                                        <div class="glyphicon glyphicon-map-marker"></div>
                                        <div>
                                            <h6>${data.result.address_components[1].long_name}</h6>
                                            <h6>${data.result.address_components[0].long_name},</h6>
                                            <h6>${data.result.address_components[2].long_name}</h6>                             
                                        </div>
                                    </div>
                                </div>
                            </div>`;

    return tilesWithPhoto;
}


