/**
 * Created by damian.warzecha on 2017.07.27.
 * Funkcja obsługi historii realizowana poprzez ukrywanie
 * elementów i wyświetlanie aktualnych
 */
function myhistory() {
    window.onpopstate = function (e) {
        if (e.state !== null){
            if (typeof e.state.url !== "undefined"){
                if (e.state.url === "tiles") {
                    addHidden(".outer");
                    addHidden(".detailsPlaces");
                    let detailsPlaces = document.querySelector(".detailsPlaces").childNodes;
                    detailsPlaces.forEach(function (item) {
                        if (item.nodeName !== "#text") {
                            item.classList.add("hidden");
                        }
                    });
                    let tilesContainer = document.querySelector(".tilesContainer").childNodes;
                    tilesContainer.forEach(function (item) {
                        if (item.nodeName !== "#text") {
                            item.classList.remove("hidden");
                        }
                    });
                }
            }

            if(typeof e.state.type !== "undefined") {
                if (e.state.type === "details") {
                    removeHidden(".outer");
                    removeHidden(".detailsPlaces");
                    let detailsPlaces = document.querySelector(".detailsPlaces").childNodes;
                    detailsPlaces.forEach(function (item) {
                        if (item.nodeName !== "#text"){
                            item.classList.add("hidden");
                        }
                    });
                    removeHidden("#DETAILS" + e.state.change);
                    google.maps.event.trigger(placesMaps[e.state.change].map, 'resize');
                    placesMaps[e.state.change].map.setCenter(placesMaps[e.state.change].pla);
                }
            }
        }
    };
}