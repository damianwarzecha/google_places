
var placesMaps = Array();

/**
 * @param places - dane z google api zawierające dane szczegołowe
 */
function tilesDetails(places) {

    /**
     * @type {null} - zmienna przechowująca informację o dostępności miejsca
     */
    let opening = null;

    /**
     * sprawdza i przypisuje do zmiennej informację o dostępności miejsca
     */
    if ( typeof places.opening_hours === 'undefined'){
        opening = 'Brak danych'
    } else if(places.opening_hours.open_now){
        opening = `Teraz otwarte`;
    } else {
        opening = `Teraz zamknięte`;
    }

    /**
     * Sprawdza i przypisuje "Brak danych" jeśli nie ma informacji w API o
     * telefonie, stronie internetowej.
     */
    if ( typeof places.international_phone_number === 'undefined'){
        places.international_phone_number = 'Brak danych'
    }
    if ( typeof places.website === 'undefined'){
        places.website = 'Brak danych'
    }

    /**
     * @type {string} - zmienna przechowująca tagi HTML do wyświetlania
     * danych szczegółowych miejsca
     */
    let placeDetailsHtml = `<table id =\"DETAILS${places.place_id}\" class="hidden">
                <tr>
                    <td class="left" rowspan="2">
                        <div class="glyphicon glyphicon-chevron-left"></div>
                    </td>
                    <td>
                        <div class="detailsMap" id="detailsMap`+places.place_id+`">
                            <!--MAPA -->
                        </div>
                    </td>
                    <td class="right" rowspan="2">
                        <div class="glyphicon glyphicon-remove"></div>
                        <div class="glyphicon glyphicon-chevron-right"></div>
                    </td>
                </tr>
                <tr>
                    <td class="det">
                        <div class="details" id="${places.place_id}">
                            <div class="detailsTiles">
                                <div class="describe">
                                    <p>${places.name}</p>
                                    <div class="col-md-6 col-sm-4">
                                        <div class="address">
                                            <div class="glyphicon glyphicon-map-marker"></div>
                                            <h6>${places.address_components[1].long_name}</h6>
                                            <h6>${places.address_components[0].long_name},</h6>
                                            <h6>${places.address_components[2].long_name}</h6>
                                        </div>
                                        <div class="glyphicon glyphicon-phone"></div>
                                        <h6>${places.international_phone_number}</h6>
                                    </div>
                                    <div class="col-md-6 col-sm-4">
                                        <div class="glyphicon glyphicon-globe"></div>
                                        <h6>${places.website}</h6>
                                        <div class="glyphicon glyphicon-time"></div>
                                        <h6>${opening}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>`

    /**
     * @type {Element} - pobranie z DOM kontenera na dane szczegółowe
     * oraz umieszczenie kolejnego elementu
     */
    let detailsPlaces = document.querySelector(".detailsPlaces");
    detailsPlaces.insertAdjacentHTML("beforeend", placeDetailsHtml);

    /**
     * @type {google.maps.Map} - utworzenie mapy z lokalizacją aktualnego miejsca
     */
    let mapa = new google.maps.Map(document.querySelector(`#detailsMap${places.place_id}`), {
        zoom: 14
    });
    let infowindow = new google.maps.InfoWindow();
    let marker = new google.maps.Marker({
        map: mapa,
        position: places.geometry.location
    });
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(places.name);
        infowindow.open(mapa, marker);
    });
    placesMaps[places.place_id] = {map: mapa, pla: places.geometry.location};
}

function clickTiles() {

    /**
     * @type {NodeList} - Pobiera wszystkie wygenerowane kafelki
     */
    let tiles = document.querySelector(".tilesContainer").childNodes;
    /**
     * @type {string} - url na potrzeby obsługi historii
     */
    let url = window.location.toString().replace("https://damianwarzecha.pl:8443/send/", "");

    /**
     * Przetwarzanie w pętli pobranych danych
     */
    tiles.forEach(function (item) {
        if(item.nodeName !== "#text"){
            /**
             * dodanie obsługi kliknięcia w widoku listy
             */
            item.addEventListener("click", function () {
                removeHidden(".outer");
                removeHidden(".detailsPlaces");
                removeHidden("#DETAILS"+item.id);
                google.maps.event.trigger(placesMaps[item.id].map, 'resize');
                placesMaps[item.id].map.setCenter(placesMaps[item.id].pla);
                window.history.pushState({type: "details", change: item.id},"","/send/"+url+"/"+item.id);
            });
            /**
             * dodanie obsługi kliknięcia przejścia do następnego miejsca w widoku szczegółowym
             */
            if(item.nextSibling!==null){
                document.querySelector("#DETAILS"+item.id+" > tbody > tr > td > .glyphicon-chevron-right")
                    .addEventListener("click", function () {
                        addHidden("#DETAILS"+item.id);
                        removeHidden("#DETAILS"+item.nextSibling.id);
                        google.maps.event.trigger(placesMaps[item.nextSibling.id].map, 'resize');
                        placesMaps[item.nextSibling.id].map.setCenter(placesMaps[item.nextSibling.id].pla);
                        window.history.pushState({type: "details", change: item.nextSibling.id},"","/send/"+url+"/"+item.nextSibling.id);
                    });
            }
            /**
             * dodanie obsługi kliknięcia przejścia do poprzedniego miejsca w widoku szczegółowym
             */
            if(item.previousSibling!==null){
                document.querySelector("#DETAILS"+item.id+" > tbody > tr > td > .glyphicon-chevron-left")
                    .addEventListener("click", function () {
                        addHidden("#DETAILS"+item.id);
                        removeHidden("#DETAILS"+item.previousSibling.id);
                        google.maps.event.trigger(placesMaps[item.previousSibling.id].map, 'resize');
                        placesMaps[item.previousSibling.id].map.setCenter(placesMaps[item.previousSibling.id].pla);
                        window.history.pushState({type: "details", change: item.previousSibling.id},"","/send/"+url+"/"+item.previousSibling.id);
                    });
            }
            /**
             * dodanie obsługi kliknięcia zamkniecia widoku szczegołowego
             */
            document.querySelector("#DETAILS"+item.id+" > tbody > tr > td > .glyphicon-remove")
                .addEventListener("click", function () {
                    addHidden(".outer");
                    addHidden("#DETAILS"+item.id);
                    addHidden(".detailsPlaces");
                    window.history.pushState({url : "tiles"}, "", "/send/"+url);
                });
        }
    });
}