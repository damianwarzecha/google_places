/**
 * Funkcja obługi przycisku uruchamiania wyszukiwania
 */
function searchPlaces() {
    /**
     * @type {Element} - pobranie z DOM tagów
     */
    let autocomplete = document.getElementById("autocomplete"),
        searchPlaces = document.getElementById("searchPlaces");
        // dataList = document.getElementById("list").childNodes;


    /**
     * Dodanie obsługi kliknięcia
     */
    searchPlaces.addEventListener("click", function () {

        let ifFind = false;
        let dataList = document.getElementById("list").childNodes;

        /**
         * Sprawdzenie w pętli czy dane wpisanę w formularzu pokrywają się z danymi
         * z podpowiedzi, jeśli tak, przekierowuje na strone listy miejsc
         */
        dataList.forEach(function (item) {
            if (item.value === autocomplete.value){
                ifFind = true;

                let type = document.querySelector("#selectType").value;
                let radius = document.querySelector("#selectRadius").value;

                window.history.pushState({url : "welcome"}, "", "/");
                window.location.replace(`/send/${item.value}/${type}/${radius}`);
            }
        });
        /**
         * Wyświetla podpowiedź po wpisaniu błędnych danych
         */
        if (!ifFind){
            let tooltip = document.querySelector(".mytooltiptext");
            tooltip.classList.remove("hidden");
        }

    });

    /**
     * Funkcja dodająca efekt wizualny błędnego wpisania danych
     */
    autocomplete.addEventListener("keyup", function () {
        autocomplete.parentNode.classList.add("has-error");
    });

    /**
     * Funkcja dodająca efekt wizualny poprawnego wpisania danych
     */
    autocomplete.addEventListener("input", function () {
            autocomplete.parentNode.classList.remove("has-error");
            autocomplete.parentNode.classList.add("has-success");
            let tooltip = document.querySelector(".mytooltiptext");
            tooltip.classList.add("hidden");
    })
}

