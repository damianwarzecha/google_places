/**
 * Uruchomienie funkcji po załadowaniu strony
 */
(function () {
    window.onload = function () {
        autocomplete();
        searchPlaces();
    }
})();