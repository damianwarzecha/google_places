/**
 * Funkcja obsługująca autopodpowiedź lokalizacji pobierająca
 * dane z Google API
 */
function autocomplete() {

    let autocompleteInput = document.getElementById("autocomplete");
    let dataList = document.getElementById("list");

    /**
     * Dodanie nasłuchiwania na zdarzenie naciśniecia i puszczenia klawisza.
     */
    autocompleteInput.addEventListener("keyup", function () {

        let self = this;

        delay(function () {
            socket.emit("autocomplete",self.value);
        },400);

    });

    /**
     * Nasłuchiwanie na odpowiedź z serwera zawierajacą dane do autopodpowiedzi
     */
    socket.on("autocomplete", function (data) {

        let option = ``;
        data.predictions.forEach(function (item, index) {
            option = option + `<option value="${item.description}" 
                                   id="${item.place_id}">`;
        });
        dataList.innerHTML = option;
    });

    /**
     * funkcja opóźniająca wykonanie żądania
     */
    let delay = (function(){
        let timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
}
