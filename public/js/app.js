/**
 * Stała przechowująca połączenie websocket
 */
const socket = io.connect("https://damianwarzecha.pl");

/**
 * nasłuchiwanie na zdarzenie końca limitu klucza do Google API
 */
socket.on("QUOTA", function (ERROR) {

    alert("Quota przekroczona, aplikacja zostanie odświeżona z nowym kluczem Google API");
    window.location.replace(`/`);

});