/**
 * Sekcja wczytywania potrzebnych bibliotek i plików
 */
const fs = require('fs');
const privKey = fs.readFileSync("ssl/ca.key","utf8");
const cert = fs.readFileSync("ssl/ca.crt","utf8");
const credentials = {key: privKey, cert: cert};
const express = require("express");
const app = express();
const server = require("https").Server(credentials, app);
const hbs = require("express-handlebars");
const io = require("socket.io")(server);
const bodyParser = require("body-parser");
const places = require("./helper/places");
const api = require("./api/api");

/**
 * Konfiguracja domyślnego szablonu widoków oraz jego rozszerzenia .hbs
 */
app.engine("hbs", hbs({defaultLayout: "main", extname: ".hbs"}));
/**
 * Ustawienie view engine
 */
app.set("view engine", "hbs");
/**
 * Konfiguracja źródła plików publicznych dla klienta
 */
app.use( express.static("public") );
/**
 * Ustawienie body parsera dla wszystkich żądań json dla aplo
 */
app.use(bodyParser.json());
/**
 * Ustawienie zewnętrznego routera żądań i przedrostka
 */
app.use("/api", api);


/**
 *
 * @type {[string,string,string,string,string]} - zbiór kluczy do Google Api
 */
let googleKeyContainer = [`AIzaSyAwTK-Yc8HdQFm2o3P4fWQKMqFjRL8kP-M`, `AIzaSyBfPjDMb4c4hkJYLFmvF-huZ9gHPr7cSK4`, 'AIzaSyD9ARvZa8yif9ZerpitUM4fqhYAtyApfdY',
                            'AIzaSyDdTvt-Nw_7qa6ZV_jDWW4Bfww86s5deaQ', 'AIzaSyAe86Dh1RW5XHB1mPQdhG4ZwdMwhVWEzcI'];
/**
 *
 * @type {number} - licznik do zmiany klucza z tablicy
 */
global.countGoogleKeys = 2;

/**
 * Zdefiniowanie scieżki /
 */

/**
 * Utworzenie socketów
 */
places(io);

app.get("/", function (req, res) {

    global.googleKey = googleKeyContainer[countGoogleKeys];
    /**
     * welcome - nazwa szablonu do wczytania
     * scripts - lista skryptów dla widoku
     */
    res.render("welcome",{
        scripts: [
            "app.js",
            "welcome/autocomplete.js",
            "welcome/welcome.js",
            "welcome/searchPlaces.js"
        ]
    });
});

app.get("/send/:loc/:type/:radius(\\d+)", function (req, res) {

    res.render("home",{
        scripts: [
            "home/myhistory.js",
            "home/tiles.js",
            "app.js",
            "home/tilesHtml.js",
            "home/tilesDetails.js",
        ]
    });
});

/**
 * Uruchomienie serwera na porcie 443
 */
server.listen(443, function () {
   console.log("Serwer uruchomiony");
});
