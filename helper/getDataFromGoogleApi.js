/**
 *  @type {request} - biblioteka wykorzystywana do wysyłania zapytań do API Googla
 */
const request = require("request");

/**
 * Funkcja pobierająca metodą radarsearch z Google Api ID miejsc na podstawie przesłanych danych
 * od klienta
 * @param url - adres okreslający miejsce w okolicy którego będą wyszukiwane miejsca
 * @param call - metoda przetwarzająca dane (ID) o miejscach z API
 * @param userData - dane przekazane przez klienta: typ, promień
 */
function getDataFromGoogleApi(url, call, userData) {

    googleRequest(url, function (data) {
        if (data.predictions !== undefined){
            if (data.predictions.length === 0){
                call("ZERO_RESULTS");
                return;
            } else {
                let getLoc = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${data.predictions[0].place_id}&key=${googleKey}`;
                googleRequest(getLoc, function (data) {
                    if (data.result !== undefined){
                        let getPlId = `https://maps.googleapis.com/maps/api/place/radarsearch/json?location=${data.result.geometry.location.lat},${data.result.geometry.location.lng}&radius=${userData.radius}&type=${userData.type}&key=${googleKey}`;
                        googleRequest(getPlId, function (data) {
                            let delay = setInterval(function () {
                                if (data.status !== undefined){
                                    if (data.status === "OK"){
                                        call(data);
                                        clearInterval(delay);
                                    } else if (data.status === "ZERO_RESULTS"){
                                        call("ZERO_RESULTS");
                                        clearInterval(delay);
                                    } else if (data === "OVER_QUERY_LIMIT") {
                                        call("OVER_QUERY_LIMIT");
                                        clearInterval(delay);
                                    } else {
                                        call("ERROR");
                                        clearInterval(delay);
                                    }
                                }
                            },10);
                        });
                    }else {
                        call("OVER_QUERY_LIMIT");
                    }
                });
            }
        }else{
            call("OVER_QUERY_LIMIT");
        }
    });

    /**
     * Funkcja wysyłająca żadanie do url, wynik zwraca do cb
     * @param url - link do Google API
     * @param cb - callback z danymi
     */
    function googleRequest(url, cb) {
        // console.log(url);
        request({
            url:url,
            json:true,
        },function (error,response,body) {
            if (!error && response.statusCode === 200){
                if (body.status === "OVER_QUERY_LIMIT"){
                    if (countGoogleKeys>=4){
                        countGoogleKeys=0;
                    }
                    countGoogleKeys++;
                    cb("OVER_QUERY_LIMIT");
                }
                // console.log(body);
                cb(body);
            } else if (error && response.statusCode !== 200){
                cb("ERROR");
            }
        });
    }
}
module.exports = getDataFromGoogleApi;
