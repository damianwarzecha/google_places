/**
 * @type {request} - biblioteka wykorzystywana do wysyłania zapytań do API Googla
 * @type {getDataFromGoogleApi} - funkcja wyszukująca ID miejsca na podstawie wprowadzonych danych o lokalizacji oraz promieniu
 * @type {getDetailsPlaces} - funkcja pobierająca dane szczegółowe na podstawie ID z getDataFromGoogleApi
 */
const request = require("request");
const getData = require("./getDataFromGoogleApi");
const getDetailsPlaces = require("./getDetailsPlaces");

/**
 * @param io - referencja do obiektu biblioteki socket.io
 */
function places(io) {

    io.on("connection", function (socket) {
        /**
         * @type {number}
         */
        let countClientRequestDetailsPlaces = 1;
        let idPlaces;

        /**
         * Funkcja wysyłająca podpowiedź lokalizacji
         */
        socket.on("autocomplete", function (data) {
            let url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${encodeURI(data)}&components=country:pl&types=geocode&language=pl_PL&key=${googleKey}`;
            /**
             * Jeśli kolejne żadanie w ciagu 100ms następuje czyszczenie Timeout`a
             */
            clearTimeout(timeout);
            var timeout = setTimeout(function () {
                request({
                    url:url,
                    json:true,
                },function (error,response,body) {
                    if (!error && response.statusCode === 200){
                        if (body.status === "OVER_QUERY_LIMIT"){
                            if (countGoogleKeys>=4){
                                countGoogleKeys=0;
                            }
                            countGoogleKeys++;
                            countGoogleKeys++;
                            socket.emit("QUOTA");
                        }
                        socket.emit("autocomplete",body);
                    }
                });
            }, 100);
        });

        socket.on("result", function (data) {
            /**
             * Sprawdzenie czy klient przesyła żądanie po raz pierwszy, jeśli tak to pobiera ID miejsc,
             * a wynik przesyła do funkcji pobierania danych szczegółowych, jeśli nie, pobiera kolejną porcje
             * danych szczegółowych
             */
            if(countClientRequestDetailsPlaces === 1){
                let url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${encodeURI(data.loc)}&components=country:pl&types=geocode&language=pl_PL&key=${googleKey}`;
                getData(url, function (data) {
                    if (data === "OVER_QUERY_LIMIT"){
                        if (countGoogleKeys>=4){
                            countGoogleKeys=0;
                        }
                        countGoogleKeys++;
                        socket.emit("QUOTA");
                    }
                    if (data.results === undefined){
                        socket.emit("checkRes", {
                            det: null,
                            loadNext: data
                        })
                    }
                    getDetailsPlaces(data, function (details, id) {
                        if (data === "OVER_QUERY_LIMIT"){
                            if (countGoogleKeys>=4){
                                countGoogleKeys=0;
                            }
                            countGoogleKeys++;
                            socket.emit("QUOTA");
                        }
                        idPlaces = id;
                        countClientRequestDetailsPlaces++;
                        socket.emit("checkRes", {
                            det: details,
                            loadNext: id
                        });
                    });
                }, data);
            } else {
                getDetailsPlaces(idPlaces, function (details, id) {
                    if (id === "OVER_QUERY_LIMIT"){
                        if (countGoogleKeys>=4){
                            countGoogleKeys=0;
                        }
                        countGoogleKeys++;
                        socket.emit("QUOTA");
                    }
                    idPlaces = id;
                    socket.emit("checkRes", {
                        det: details,
                        loadNext: id
                    });
                });
            }
        })
    });
}

module.exports = places;