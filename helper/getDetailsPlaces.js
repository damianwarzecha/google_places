/**
 *  @type {request} - biblioteka wykorzystywana do wysyłania zapytań do API Googla
 */
const request = require("request");


/**
 * @param data - dane z API Google zawierajace ID miejsc wygenerowane z klasy getDataFromGoogleApi
 * @param call - callback z danymi oraz statusem pobierania danych szczegółowych
 */
function getDetailsPlaces(data, call) {

    /**
     * Wywołanie funkcji
     */
    forE(data,function (detailsPlaces, status) {
        call(detailsPlaces, status);
    }, new Array(),0, 18);


    /**
     * @param placeData - dane z API Google zawierajace ID miejsc wygenerowane z klasy getDataFromGoogleApi
     * @param cb - callback z danymi oraz statusem pobierania danych szczegółowych
     * @param array - kontener na dane szczegółowe
     * @param index - licznik
     * @param countLoad - liczba ładowanych danych
     */
    function forE(placeData, cb, array, index, countLoad) {
        if(index < countLoad){
            /**
             * Odczytywanie w petli danych szczegółowych z API
             */
            setTimeout(function () {
                /**
                 * jeśli nie ma zadnych danych w tablicy to wychodzi z pętli
                 */
                if (placeData.results === undefined){
                    return;
                }
                /**
                 * @type {string} - link do API ze zmienna placeid
                 */
                let getDet = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeData.results[0].place_id}&key=${googleKey}`;
                /**
                 * request do API z funkcja zwrotna dodajaca link do zdjecia jeśli takowe istnieje
                 * i dodanie wyniku do tablicy, nastepnie usuwa biezacy element z tablicy i uruchamia kolejny przebieg
                 * petli, w przypadku bledu wykonuje request ponownie z tymi samymi danymi.
                 *
                 * jeśli tablica jest pusta, zwraca wynik do funkcji cb, z informacja o zakonczeniu przetwarzania tablicy
                 */
                googleRequest(getDet, function (details) {
                    if (details !== "ERROR"){
                        if (details === "OVER_QUERY_LIMIT") {
                            cb(null, "OVER_QUERY_LIMIT");
                            return;
                        }
                        if(details.result !== undefined){
                            if(details.result.photos !== undefined){
                                details.photoUrl = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=${details.result.photos[0].photo_reference}&key=${googleKey}`;
                            }
                            array.push(details);
                            /**
                             * usuwa z tablicy przetworzony ID
                             */
                            data.results.splice(0, 1);
                            if (data.results.length === 0){
                                cb(array,"END");
                                return;
                            }
                            forE(placeData, cb, array, ++index, 18);
                        }
                    } else {
                        forE(placeData, cb, array, index, 18);
                    }
                });
            },10);
            /**
             * jeśli licznik == 18 zwraca część przetworzonych danych
             */
        } else {
            cb(array, data);
        }
    }

    /**
     * Funkcja wysyłająca żadanie do url, wynik zwraca do cb
     * @param url - link do Google API
     * @param cb - callback z danymi
     */
    function googleRequest(url, cb) {
        request({
            url:url,
            json:true,
        },function (error,response,body) {
            if (!error && response.statusCode === 200){
                if (body.status === "OVER_QUERY_LIMIT"){
                    if (countGoogleKeys>=4){
                        countGoogleKeys=0;
                    }
                    countGoogleKeys++;
                    cb("OVER_QUERY_LIMIT");
                }
                cb(body);
            } else if (error && response.statusCode !== 200){
                cb("ERROR");
            }
        });
    }
}

/**
 * Export modułu
 * @type {getDetailsPlaces}
 */
module.exports = getDetailsPlaces;