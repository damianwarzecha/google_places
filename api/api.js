const express = require("express");
const model = require("../model/places");

const router = express.Router();



router.get("/", function (req, res) {
    res.render("api", {places : model.list(),
        scripts: ["api/api.js"]
    });
});

router.post("/", function (req, res) {
    model.add(req.body);
});

router.delete("/:id", function (req, res) {
    model.delete(req.params.id);
});

router.put("/:id", function (req, res) {
    req.body.id = req.params.id;
    model.update(req.body);
});

module.exports = router;